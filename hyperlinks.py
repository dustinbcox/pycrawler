#!/usr/bin/env python2.7
"""Hyperlinks simple web crawler coding challenge

.. moduleauthor:: Dustin Cox <dustin@dustinbcox.com>

"""

import argparse
import collections
import json
import logging
import pymongo
import sys
import urllib3
import urlparse

from webdataobjectmemory import WebDataObjectMemory


class Hyperlinks(object):
    """ Web crawler """
    def __init__(self, url, limit):
        """constructor.

        Args:
            url (str): starting url
            limit(str): maximum number of web requests

        """
        self._start_url = url
        self._limit = limit
        self._http = urllib3.PoolManager()
        self._links = {}
        self._queue = collections.deque()
        self._urls_accessed = set()
        if limit <= 0:
            raise ValueError("limit is expected to be greater than 0")

    def crawl(self):
        """ Method to start the crawling process """
        logging.info("Started crawl on {0}".format(self._start_url))
        self._queue.append(self._start_url)
        while len(self._queue) > 0 and self._limit > 0:
            url = self._queue.popleft()
            if url not in self._urls_accessed:
                logging.debug("GET {0}".format(url))
                web_data = self.get(url)
                self._limit -= 1
                if web_data is not None:
                    links = web_data.parse_anchors()
                    logging.debug("- Found {0} links: {1}".
                                  format(len(links), " ".join(links)))
                    self._queue.extend(links)
                    self._populate_links(url, links)
            else:
                logging.debug("Ignoring url={0} since we already followed it".
                              format(url))
        logging.info("Finished crawl on {0}".format(self._start_url))

    def get(self, url):
        """ HTTP GET on url.
        Args:
            url (str): url to GET

        Returns:
            WebDataObjectMemory or None on error

        """
        res = self._http.request('GET', url)
        if res.status == 200:
            if 'content-type' in res.headers:
                # Handle sloppy web servers
                if 'text/html' in res.headers['content-type'].lower():
                    return WebDataObjectMemory(mime='text/html', data=res.data)
                else:
                    logging.warning("{0} : unexpected content-type {1}".
                                    format(url, res.headers['content-type']))
                    print res.headers['content-type']
            else:
                logging.warning("Content-type not found in {0}, bad server".
                                format(url))
        else:
            logging.warning("Failed to GET {0}".format(url))

    def output_json(self, stream):
        """produce the output to stream

        Args:
            stream (filehandle): Stream for outputting pretty json.

        """
        json.dump(self._links, stream, sort_keys=True, indent=4)

    def output_mongo(self, mongo):
        """Output link structure to mongo

        Args:
            mongo (str): Uri for connecting to mongo.

        """
        # TODO add indexing to domain
        mongo_uri = urlparse.urlparse(mongo)
        client = pymongo.MongoClient("{0}://{1}/".format(mongo_uri.scheme,
                                                         mongo_uri.netloc))
        (database_name, collection_name) = mongo_uri.path.split('/')[1:]
        collection = client[database_name][collection_name]
        for domain in self._links:
            doc = self._links[domain]
            doc['domain'] = domain
            collection.update({'_id': domain}, doc, upsert=True)

    def _populate_links(self, url, links):
        """Internal method to populate the link graph structure
    
        Args:
            url (str): url of the current page
            links (list): list of urls (str) found on page

        """
        current_domain = urlparse.urlparse(url)[1]
        for link in links:
            link_domain = urlparse.urlparse(link)[1]
            if current_domain != link_domain:
                self._add_link(current_domain, 'outgoing', link)
                self._add_link(link_domain, 'incoming', current_domain)
            else:
                logging.debug("Ignore {0} links back to itself {1}".
                              format(url, link))

    def _add_link(self, domain, direction, url):
        """Internal method to create the digraph relationship between domain
        and url.

        Args:
            domain(str): fqdn or just domain name
            direction(str): lowercase 'incoming' or 'outgoing'
            url(str): full url

        """
        assert direction == 'incoming' or direction == 'outgoing'
        if domain in self._links:
            links = self._links[domain][direction]
            if not url in links:
                links.append(url)
                self._links[domain][direction] = links
        else:
            self._links[domain] = {'incoming': [], 'outgoing': []}


def main():
    """ Main driver, argument parsing"""
    parser = argparse.ArgumentParser(description="PyCrawler hyperlinks")
    parser.add_argument('-u', '--url', required=True, help="Start URL")
    parser.add_argument('-l', '--limit', required=True, type=int,
                        help="Maximum number of domains to crawl")
    parser.add_argument('-o', '--out', help="Output file")
    parser.add_argument('--dbout', help="MongoDB Connection Uri: " +
                        "ex: 'mongodb://localhost:27017/database/collection'")
    parser.add_argument('--log', default='hyperlinks.log',
                        help="Log output to file")
    args = parser.parse_args()

    logging.basicConfig(filename=args.log, level=logging.DEBUG)
    crawler = Hyperlinks(url=args.url, limit=args.limit)
    crawler.crawl()
    if args.out is not None:
        with open(args.out, 'w+') as filehandle:
            crawler.output_json(filehandle)
    else:
        crawler.output_json(sys.stdout)

    if args.dbout is not None:
        crawler.output_mongo(args.dbout)

if __name__ == "__main__":
    main()
