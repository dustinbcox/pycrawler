hyperlinks.py utility
=====================

.. automodule:: hyperlinks
   :members:


Web Data Object in Memory
=========================

.. automodule:: webdataobjectmemory
   :members:



Contents:

.. toctree::
   :maxdepth: 2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

