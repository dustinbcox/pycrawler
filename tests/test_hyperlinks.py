"""Unit tests for Hyperlinks class"""

import unittest
import hyperlinks
from mock import patch, Mock, mock_open

FAKE_HTML_PAGE = """
<!DOCTYPE html>
<html lang="en" charset="UTF-8">
<head><title>FakePage</title><body>
<a data-whatever="fake" hReF='http://www.enterprise.com/' > thing </a>
<A href="http://www.enterprise.com"></A>
</body></html>"""


def mock_urllib3_helper(mock, data, status=200, mime='text/html'):
    """ return manager,request """
    manager = mock.return_value
    request = manager.request.return_value
    request.status = status
    if mime is not None:
        request.headers = {'content-type': mime}
    else:
        request.headers = {}
    request.data = data
    return manager, request


class TestHyperlinks(unittest.TestCase):
    """ Unit testing for Hyperlinks """
    @patch('hyperlinks.urllib3.PoolManager')
    def test_crawl(self, mock):
        """ Test crawl function """
        url = 'somewhere'
        (manager, request) = mock_urllib3_helper(mock, FAKE_HTML_PAGE)
        crawler = hyperlinks.Hyperlinks(url, 1)
        self.assertIs(crawler._http, manager)
        self.assertEqual(crawler._limit, 1)
        self.assertEqual(crawler._start_url, url)

    @patch('hyperlinks.urllib3.PoolManager')
    @patch('hyperlinks.logging')
    def test_get(self, mock_logging, mock_poolman):
        """ Test web get method """
        url = 'nowhere'
        (manager, request) = mock_urllib3_helper(mock_poolman, FAKE_HTML_PAGE)
        crawler = hyperlinks.Hyperlinks(url, 1)
        web_data = crawler.get(url)
        manager.request.assert_called_with('GET', url)
        (manager, request) = mock_urllib3_helper(mock_poolman, FAKE_HTML_PAGE,
                                                 mime=None)
        mock_logging.warning.return_value = Mock()
        web_data = crawler.get(url)
        self.assertTrue(mock_logging.warning.called)

    @patch('hyperlinks.Hyperlinks')
    @patch('hyperlinks.argparse.ArgumentParser')
    @patch('hyperlinks.logging.basicConfig')
    def test_main(self, mock_logging, mock_parser, mock_crawler):
        """ Test the main driver """
        mock_crawler.crawl.return_value = True
        mock_open_call = mock_open()
        with patch('__builtin__.open', mock_open_call, create=True):
            parser = mock_parser.return_value
            parser.parse_args.return_value = Mock(url='www', limit='4',
                                                  log='123.log')
            hyperlinks.main()
            self.assertEqual(len(parser.add_argument.mock_calls), 5)
            mock_logging.assert_called_once_with(filename='123.log',
                                                 level=hyperlinks.logging.
                                                 DEBUG)
            mock_crawler.assert_called_once_with(url='www', limit='4')
            dbout = 'mongodb://localhost:27017/db/coll'
            arguments = Mock(log='a.log', url='www', limit=4,
                             dbout=dbout,
                             out='adf')
            parser.parse_args.return_value = arguments
            hyperlinks.main()
