"""Unit testing for WebDataObjectMemory"""
import unittest
from webdataobjectmemory import WebDataObjectMemory

HTML_MIME = 'text/html'


class TestWebDataObjectMemory(unittest.TestCase):
    """ Test the Web Data Object Memory Class """
    def test_properties(self):
        """ Test the properties of the class """
        data = '<html></html>'
        webobj = WebDataObjectMemory(HTML_MIME, data)
        self.assertEquals(webobj.mime, HTML_MIME)
        self.assertEquals(webobj.data, data)

    def test_parse_anchors(self):
        """ Test the anchors """
        anchor_data = """
            < a HREF="www.http.something.nu" > </a>
            < A href  =  'www.fakesite.com'></a>
            <  a href="mailto:no@mail.com" />
            <A HREF = http://www.something >
        """
        webobj = WebDataObjectMemory(HTML_MIME, anchor_data)
        links = webobj.parse_anchors()
        self.assertEqual(links, ['www.http.something.nu', 'www.fakesite.com',
                                 'http://www.something'])
