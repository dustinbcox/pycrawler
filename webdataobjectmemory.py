"""Simple class for containing a web object (i.e. html)

.. moduleauthor:: Dustin Cox <dustin@dustinbcox.com>

"""
import re


class WebDataObjectMemory(object):
    """ Store a full web object, eg. html, css, text whatever in memory and
    parses it

    .. note:: Currently :func:`parse_anchors` uses a primitive regex and
    in the future a proper html parser such as BeautifulSoup, html5lib, lxml,
    etc. I used a regex as the instructions suggested to use one. The
    reason for using a proper parser (and tokenizer) is there are many
    more corner cases than can be reasonably matched here, e.g. html5
    data, css attributes, bad html. So this is a good enough regex for
    parsing html

    """
    def __init__(self, mime, data):
        """constructor, requires mime (i.e. text/html) and data.

        Args:
            mime (str): content-type mime format in lower case.
            data (str): content of the web object

        """
        self._html_a_regex = re.compile(
            r"""< *a (?:[\w]+ *= *['"]?[\w]+['"]?)* """ +
            r"""*href *= *['"]?((?:https?|www)[\w/\.:]+)['"]?""",
            re.IGNORECASE | re.DOTALL | re.MULTILINE)
        self.mime = mime
        self.data = data

    def parse_anchors(self):
        """iparse anchors <a href='website'></a> out of html.

        Returns:
         list of urls

        """
        matches = self._html_a_regex.findall(self.data)
        return matches
